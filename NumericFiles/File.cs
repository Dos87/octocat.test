﻿using System.IO;

namespace OctoCat.Test.NumericFiles
{
    public class File : INumericFile
    {
        public string Name { get; }
        public string Dir { get; }
        public string[] Lines { get; }
        public string FullName => Path.Combine(Dir, Name);

        public File(string name, string dir, string[] lines)
        {
            Name = name;
            Dir = dir;
            Lines = lines;
        }
    }
}
