﻿namespace OctoCat.Test.NumericFiles
{
    public class FileFactory : INumericFileFactory
    {
        public INumericFile Create(string name, string dir, string[] lines)
        {
            return new File(name, dir, lines);
        }
    }
}
