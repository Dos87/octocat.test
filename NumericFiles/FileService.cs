﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OctoCat.Test.Exceptions;
using OctoCat.Test.Log;
using OctoCat.Test.Settings;

namespace OctoCat.Test.NumericFiles
{
    public class FileService : INumericFileService
    {
        /// <summary>Служба занята</summary>
        public bool IsBusy { get; private set; }

        private readonly ILogService _logService;
        private readonly INumericFileFactory _fileFactory;
        private readonly ISettingsService _settingsService;

        /// <summary>Задержка в миллисекундах для публикации прогресса (чтобы не повесить UI)</summary>
        private const int ProgressPublishDelay = 10;
        /// <summary>Последнее время публикации прогресса</summary>
        private DateTime _lastProgressTime = DateTime.MinValue;

        public FileService(ILogService logService, INumericFileFactory fileFactory, ISettingsService settingsService)
        {
            _logService = logService;
            _fileFactory = fileFactory;
            _settingsService = settingsService;
        }

        public async Task GenerateAsync(string dir, IProgress<GenerationProgressReport> progress, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(dir))
                throw new ArgumentException("You must specify a directory");

            if (IsBusy)
                throw new FileGeneratorException("File generator is busy and cannot process another action");

            try
            {
                IsBusy = true;

                var dirs = new List<string>();

                var progressReport = new GenerationProgressReport
                {
                    TotalProgressAmount = _settingsService.GeneratorSettings.FilesCount,
                    CurrentProgressMessage = "Generating directories..."
                };
                progress?.Report(progressReport);

#pragma warning disable CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод
                await Task.Run(async () =>
                {
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                    else if (Directory.EnumerateFileSystemEntries(dir).Any())
                        throw new FileGeneratorException($"Generation is not possible until the directory '{dir}' is not empty!");

                    GenerateSubDirs(dir, _settingsService.GeneratorSettings.MaxSubDirDepth, cancellationToken);
                    LoadAllDirsRecursively(dir, dirs);
                }, cancellationToken);

                progressReport.CurrentProgressMessage = "Generating files...";
                progress?.Report(progressReport);
                var fileNumberCounter = -1;

                var sw = Stopwatch.StartNew();
                var tasks = Enumerable.Range(0, _settingsService.GeneratorSettings.GeneratingThreadsCount).Select(t => Task.Run(async () =>
                {
                    var random = new Random();

                    for (var fileNumber = Interlocked.Increment(ref fileNumberCounter); fileNumber < _settingsService.GeneratorSettings.FilesCount; fileNumber = Interlocked.Increment(ref fileNumberCounter))
                    {
                        cancellationToken.ThrowIfCancellationRequested();

                        //Create a random file in a random directory
                        var fileDir = dirs[random.Next(0, dirs.Count - 1)];
                        var file = GenerateRandomFile(fileNumber, fileDir);
                        System.IO.File.WriteAllLines(file.FullName, file.Lines, Encoding.UTF8);
                        lock (progressReport)
                        {
                            progressReport.CurrentProgressAmount++;
                            progressReport.TimeElapsed = sw.Elapsed;

                            if (DateTime.Now - _lastProgressTime > TimeSpan.FromMilliseconds(ProgressPublishDelay))
                            {
                                _lastProgressTime = DateTime.Now;
                                progress?.Report(progressReport);
                            }
                        }
                    }
                }, cancellationToken));
#pragma warning restore CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод

                await Task.WhenAll(tasks);
                sw.Stop();
                progressReport.TimeElapsed = sw.Elapsed;
                progressReport.CurrentProgressMessage = "Generation completed successfully";
                progress?.Report(progressReport);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public async Task CalculateAsync(string dir, IProgress<CalculationProgressReport> progress, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(dir))
                throw new ArgumentException("You must specify a directory");
            if (!Directory.Exists(dir))
                throw new FileCalcuationException($"Directory '{dir}' is not exist!");
            if (!Directory.EnumerateFileSystemEntries(dir).Any())
                throw new FileCalcuationException($"Calculation is not possible until the directory '{dir}' is empty!");

            if (IsBusy)
                throw new FileCalcuationException("Calculator is busy and cannot process another action");

            try
            {
                IsBusy = true;

                var progressReport = new CalculationProgressReport { CurrentProgressMessage = "Counting files..." };
                progress?.Report(progressReport);
                var files = new List<string>();

#pragma warning disable CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод
                await Task.Run(async () =>
                {
                    LoadAllFilesRecursively(dir, files, cancellationToken);
                    cancellationToken.ThrowIfCancellationRequested();
                    progressReport.TotalProgressAmount = files.Count;
                    progressReport.CurrentProgressMessage = "Calculating...";
                    progress?.Report(progressReport);
                }, cancellationToken);

                var fileNumberCounter = -1;
                var sw = Stopwatch.StartNew();
                var tasks = Enumerable.Range(0, _settingsService.CalculatorSettings.CalculatingThreadsCount).Select(t => Task.Run(async () =>
                {
                    for (var fileNumber = Interlocked.Increment(ref fileNumberCounter); fileNumber < files.Count && !cancellationToken.IsCancellationRequested; fileNumber = Interlocked.Increment(ref fileNumberCounter))
                    {
                        var filePath = files[fileNumber];
                        var result = CalculateFileSum(filePath);

                        lock (progressReport)
                        {
                            progressReport.CurrentProgressAmount++;
                            progressReport.TimeElapsed = sw.Elapsed;
                            if (!result.IsFileSkipped)
                            {
                                progressReport.SkippedLinesCount += result.SkippedLinesCount;
                                progressReport.CurrentSum += result.Sum;
                                progressReport.EmptyLinesCount += result.EmptyLinesCount;
                            }
                            else
                                progressReport.SkippedFilesCount++;

                            if (DateTime.Now - _lastProgressTime > TimeSpan.FromMilliseconds(ProgressPublishDelay))
                            {
                                _lastProgressTime = DateTime.Now;
                                progress?.Report(progressReport);
                            }
                        }
                    }
                    cancellationToken.ThrowIfCancellationRequested();
                }, cancellationToken));
#pragma warning restore CS1998 // В асинхронном методе отсутствуют операторы await, будет выполнен синхронный метод

                await Task.WhenAll(tasks);
                sw.Stop();
                progressReport.TimeElapsed = sw.Elapsed;
                progressReport.CurrentProgressMessage = "Calculation completed successfully";
                progress?.Report(progressReport);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void LoadAllDirsRecursively(string dir, List<string> container)
        {
            container.Add(dir);
            var subdirs = Directory.EnumerateDirectories(dir);
            foreach (var subdir in subdirs)
                LoadAllDirsRecursively(subdir, container);
        }

        private void LoadAllFilesRecursively(string dir, List<string> container, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
                return;
            var files = Directory.EnumerateFiles(dir);
            container.AddRange(files);
            var subdirs = Directory.EnumerateDirectories(dir);
            foreach (var subdir in subdirs)
                LoadAllFilesRecursively(subdir, container, cancellationToken);
        }

        /// <summary>Recursive method to generate subdirectories</summary>
        private void GenerateSubDirs(string baseDir, int depth, CancellationToken cancellationToken)
        {
            if (depth < 0)
                return;

            cancellationToken.ThrowIfCancellationRequested();

            var random = new Random();

            var subDirsCount = random.Next(0, _settingsService.GeneratorSettings.MaxSubDirsCount);
            for (var i = 0; i < subDirsCount; i++)
            {
                var newDir = Path.Combine(baseDir, i.ToString());
                Directory.CreateDirectory(newDir);
                if (depth > 0)
                    GenerateSubDirs(newDir, random.Next(0, depth - 1), cancellationToken);
            }
        }

        private INumericFile GenerateRandomFile(int fileNumber, string dir)
        {
            var random = new Random();
            var linesCount = random.Next(0, _settingsService.GeneratorSettings.MaxLinesCount);
            var lines = new string[linesCount];
            for (var i = 0; i < linesCount; i++)
            {
                var isEmptyString = _settingsService.GeneratorSettings.ChanceOfEmptyString >= random.Next(1, 100);
                if (isEmptyString)
                    lines[i] = string.Empty;
                else
                {
                    var sb = new StringBuilder();
                    var haveLeftSpaces = _settingsService.GeneratorSettings.ChanceOfSpaceInTheLine >= random.Next(1, 100);
                    if (haveLeftSpaces)
                    {
                        var leftSpacesCount = random.Next(1, _settingsService.GeneratorSettings.MaxSpacesCountInLine);
                        sb.Append(' ', leftSpacesCount);
                    }
                    var lineValue = random.Next(int.MinValue, int.MaxValue);
                    sb.Append(lineValue);
                    var haveRightSpaces = _settingsService.GeneratorSettings.ChanceOfSpaceInTheLine >= random.Next(1, 100);
                    if (haveRightSpaces)
                    {
                        var rightSpacesCount = random.Next(1, _settingsService.GeneratorSettings.MaxSpacesCountInLine);
                        sb.Append(' ', rightSpacesCount);
                    }

                    lines[i] = sb.ToString();
                }
            }

            return _fileFactory.Create(fileNumber + ".txt", dir, lines);
        }

        private CalculateFileSumResult CalculateFileSum(string filePath)
        {
            var result = new CalculateFileSumResult();

            var fi = new FileInfo(filePath);
            var fileSize = (ulong)fi.Length;
            var freeRam = MemoryStatus.GetFreeRam();
            //Если файл не влазит в оперативку, то пропустим его
            if (fileSize >= freeRam)
            {
                result.IsFileSkipped = true;
                _logService.Warn($"Failed to parse file \"{filePath}\" because the file size ({fileSize}bytes) much more available RAM ({freeRam}bytes). File was skipped");
                return result;
            }

            string fileContent;
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var sr = new StreamReader(fs))
                {
                    //Загрузим весь файл в оперативку - так быстрее
                    fileContent = sr.ReadToEnd();
                }
            }
            using (var sr = new StringReader(fileContent))
            {
                var line = sr.ReadLine();
                while (line != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                        result.EmptyLinesCount++;
                    else
                    {
                        line = line.Trim();
                        long lineNumber;
                        if (long.TryParse(line, out lineNumber))
                            result.Sum += lineNumber;
                        else
                        {
                            result.SkippedLinesCount++;
                            _logService.Warn($"Failed to parse line \"{line}\" in file \"{filePath}\". This line was skipped.");
                        }
                    }
                    line = sr.ReadLine();
                }
            }
            return result;
        }

        public async Task CleanDirAsync(string directory)
        {
            await Task.Factory.StartNew(() =>
            {
                var dirInfo = new DirectoryInfo(directory);
                foreach (var file in dirInfo.EnumerateFiles())
                    file.Delete();
                foreach (var dir in dirInfo.EnumerateDirectories())
                    dir.Delete(true);
            });
        }

        private class CalculateFileSumResult
        {
            internal long Sum;
            internal int SkippedLinesCount;
            internal int EmptyLinesCount;
            internal bool IsFileSkipped;
        }
    }
}
