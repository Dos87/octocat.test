﻿using System.Runtime.InteropServices;

namespace OctoCat.Test.NumericFiles
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    internal class MemoryStatus
    {
        public uint dwLength;
        public uint dwMemoryLoad;
        public ulong ullTotalPhys;
        public ulong ullAvailPhys;
        public ulong ullTotalPageFile;
        public ulong ullAvailPageFile;
        public ulong ullTotalVirtual;
        public ulong ullAvailVirtual;
        public ulong ullAvailExtendedVirtual;
        public MemoryStatus()
        {
            dwLength = (uint)Marshal.SizeOf(typeof(MemoryStatus));
        }
        
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool GlobalMemoryStatusEx([In, Out] MemoryStatus lpBuffer);

        public static ulong GetTotalRam()
        {
            var mem = new MemoryStatus();

            GlobalMemoryStatusEx(mem);

            return mem.ullTotalPhys;
        }

        public static ulong GetFreeRam()
        {
            var mem = new MemoryStatus();

            GlobalMemoryStatusEx(mem);

            return mem.ullAvailPhys;
        }
    }
}
