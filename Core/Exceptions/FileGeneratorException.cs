﻿using System;

namespace OctoCat.Test.Exceptions
{
    public class FileGeneratorException : Exception
    {
        public FileGeneratorException(string message) : base(message)
        {
        }

        public FileGeneratorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
