﻿using System;

namespace OctoCat.Test.Exceptions
{
    public class FileCalcuationException : Exception
    {
        public FileCalcuationException(string message) : base(message)
        {
        }

        public FileCalcuationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
