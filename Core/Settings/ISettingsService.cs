﻿namespace OctoCat.Test.Settings
{
    /// <summary>Служба настроек</summary>
    public interface ISettingsService
    {
        /// <summary>Основные (общие) настройки</summary>
        IGeneralSettings GeneralSettings { get; }
        /// <summary>Настройки генератора</summary>
        IGeneratorSettings GeneratorSettings { get; }
        /// <summary>Настройки калькулятора</summary>
        ICalculatorSettings CalculatorSettings { get; }
    }
}
