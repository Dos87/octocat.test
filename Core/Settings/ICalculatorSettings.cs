﻿namespace OctoCat.Test.Settings
{
    public interface ICalculatorSettings
    {
        /// <summary>Количество потоков для выполнения подсчёта</summary>
        int CalculatingThreadsCount { get; }
    }
}
