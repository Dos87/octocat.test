﻿namespace OctoCat.Test.Settings
{
    public interface IGeneratorSettings
    {
        /// <summary>Количество потоков для генерации файловой структуры</summary>
        int GeneratingThreadsCount { get; set; }
        /// <summary>Количество файлов, которое должно быть сгенерировано</summary>
        int FilesCount { get; set; }
        /// <summary>Максимальное количество линий в файле</summary>
        int MaxLinesCount { get; set; }
        /// <summary>Максимальное количество подпапок на любой глубине</summary>
        int MaxSubDirsCount { get; set; }
        /// <summary>Максимальная глубина создания подпапок</summary>
        int MaxSubDirDepth { get; set; }
        /// <summary>Максимальное количество пробелов в начале или в конце линии</summary>
        int MaxSpacesCountInLine { get; set; }
        /// <summary>Вероятность создания пустой строки (от 0 до 100)</summary>
        int ChanceOfEmptyString { get; set; }
        /// <summary>Вероятность того, что в начале или в конце линии будут пробелы (от 0 до 100)</summary>
        int ChanceOfSpaceInTheLine { get; set; }
    }
}
