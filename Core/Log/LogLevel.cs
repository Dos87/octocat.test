﻿namespace OctoCat.Test.Log
{
    public enum LogLevel
    {
        Debug,
        Info,
        Success,
        Warn,
        Error
    }
}
