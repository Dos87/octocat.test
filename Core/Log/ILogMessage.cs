﻿using System;

namespace OctoCat.Test.Log
{
    public interface ILogMessage: IComparable
    {
        LogLevel LogLevel { get; }
        Exception Exception { get; }
        string Message { get; }
        DateTime DateTime { get; }
    }
}
