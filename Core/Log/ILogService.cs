﻿using System;

namespace OctoCat.Test.Log
{
    public interface ILogService
    {
        /// <summary>Возникает при любой записи лога</summary>
        event EventHandler<LogEventArgs> OnLogWritten;

        void Debug(string message);
        void Info(string message);
        void Success(string message);
        void Warn(string message);
        void Warn(Exception exception);
        void Warn(Exception exception, string message);
        void Error(string message);
        void Error(Exception exception);
        void Error(Exception exception, string message);
        void Log(ILogMessage logMessage);
    }
}
