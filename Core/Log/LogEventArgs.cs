﻿using System;

namespace OctoCat.Test.Log
{
    public class LogEventArgs : EventArgs
    {
        public ILogMessage LogMessage { get; }

        public LogEventArgs(ILogMessage logMessage)
        {
            LogMessage = logMessage;
        }
    }
}
