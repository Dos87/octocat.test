﻿using System;

namespace OctoCat.Test.NumericFiles
{
    public class CalculationProgressReport
    {
        public int CurrentProgressAmount { get; set; }
        public int TotalProgressAmount { get; set; }
        public string CurrentProgressMessage { get; set; }
        public int SkippedLinesCount { get; set; }
        public int EmptyLinesCount { get; set; }
        public int SkippedFilesCount { get; set; }
        public long CurrentSum { get; set; }
        public TimeSpan TimeElapsed { get; set; }
    }
}
