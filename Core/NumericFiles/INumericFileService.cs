﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace OctoCat.Test.NumericFiles
{
    /// <summary>Служба для работы с файлами согласно тестовому заданию</summary>
    public interface INumericFileService
    {
        /// <summary>Служба занята</summary>
        bool IsBusy { get; }

        /// <summary>Сгенерировать структуру папок и файлов согласно тестовому заданию</summary>
        /// <param name="dir">Директория, в которой будет сгенерирована структура</param>
        /// <param name="progress">Прогресс</param>
        /// <param name="cancellationToken">Токен отмены</param>
        Task GenerateAsync(string dir, IProgress<GenerationProgressReport> progress, CancellationToken cancellationToken);

        /// <summary>Посчитать сумму значений строк фо всех файлах согласно тестовому заданию</summary>
        /// <param name="dir">Директория, в которой рекурсивно будут загружены и посчитаны все файлы</param>
        /// <param name="progress">Прогресс</param>
        /// <param name="cancellationToken">Токен отмены</param>
        Task CalculateAsync(string dir, IProgress<CalculationProgressReport> progress, CancellationToken cancellationToken);

        /// <summary>Очистить директорию и удалить все файлы и папки в ней</summary>
        /// <param name="dir">Директория</param>
        Task CleanDirAsync(string dir);
    }
}
