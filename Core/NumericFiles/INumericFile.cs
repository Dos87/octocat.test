﻿namespace OctoCat.Test.NumericFiles
{
    public interface INumericFile
    {
        string Name { get; }
        string FullName { get; }
        string Dir { get; }
        /// <summary>Content</summary>
        string[] Lines { get; }
    }
}
