﻿using System;

namespace OctoCat.Test.NumericFiles
{
    public class GenerationProgressReport
    {
        public int CurrentProgressAmount { get; set; }
        public int TotalProgressAmount { get; set; }
        public string CurrentProgressMessage { get; set; }
        public TimeSpan TimeElapsed { get; set; }
    }
}
