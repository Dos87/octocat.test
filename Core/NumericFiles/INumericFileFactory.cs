﻿namespace OctoCat.Test.NumericFiles
{
    public interface INumericFileFactory
    {
        /// <summary>Create new <see cref="INumericFile"/></summary>
        /// <param name="name">The short name of the file</param>
        /// <param name="dir">The directory that contains this file</param>
        /// <param name="lines">The file content as string lines</param>
        INumericFile Create(string name, string dir, string[] lines);
    }
}
