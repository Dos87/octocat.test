﻿using System;
using NLog;
using OctoCat.Test.Log;
using LogLevel = OctoCat.Test.Log.LogLevel;
using NLogLevel = NLog.LogLevel;

namespace Log.NLog
{
    public class NLogService: ILogService
    {
        private readonly Logger _logger;

        public event EventHandler<LogEventArgs> OnLogWritten;

        public NLogService()
        {
            _logger = LogManager.GetLogger("LogService");
        }

        public void Debug(string message)
        {
            Log(new NLogMessage(LogLevel.Debug, null, message));
        }

        public void Info(string message)
        {
            Log(new NLogMessage(LogLevel.Info, null, message));
        }

        public void Success(string message)
        {
            Log(new NLogMessage(LogLevel.Success, null, message));
        }

        public void Warn(string message)
        {
            Log(new NLogMessage(LogLevel.Warn, null, message));
        }

        public void Warn(Exception exception)
        {
            Log(new NLogMessage(LogLevel.Warn, exception, null));
        }

        public void Warn(Exception exception, string message)
        {
            Log(new NLogMessage(LogLevel.Warn, exception, message));
        }

        public void Error(string message)
        {
            Log(new NLogMessage(LogLevel.Error, null, message));
        }

        public void Error(Exception exception)
        {
            Log(new NLogMessage(LogLevel.Error, exception, null));
        }

        public void Error(Exception exception, string message)
        {
            Log(new NLogMessage(LogLevel.Error, exception, message));
        }

        public void Log(ILogMessage logMessage)
        {
            _logger.Log(ConvertLogLevel(logMessage.LogLevel), logMessage.Exception, logMessage.Message);
            OnLogWritten?.Invoke(this, new LogEventArgs(logMessage));
        }

        private NLogLevel ConvertLogLevel(LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Debug:
                    return NLogLevel.Debug;
                case LogLevel.Info:
                    return NLogLevel.Info;
                case LogLevel.Success:
                    return NLogLevel.Info;
                case LogLevel.Warn:
                    return NLogLevel.Warn;
                case LogLevel.Error:
                    return NLogLevel.Error;
                default:
                    return NLogLevel.Fatal;
            }
        }
    }
}
