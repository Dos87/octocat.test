﻿using System;
using OctoCat.Test.Log;

namespace Log.NLog
{
    public class NLogMessage : ILogMessage
    {
        public LogLevel LogLevel { get; }
        public Exception Exception { get; }
        public string Message { get; }
        public DateTime DateTime { get; }

        public NLogMessage(LogLevel logLevel, Exception exception, string message)
            : this(logLevel, exception, message, DateTime.Now) { }

        public NLogMessage(LogLevel logLevel, Exception exception, string message, DateTime dateTime)
        {
            LogLevel = logLevel;
            Exception = exception;
            Message = message;
            DateTime = dateTime;
        }

        public int CompareTo(object obj)
        {
            var anotherLogMessage = (ILogMessage) obj;
            return DateTime.CompareTo(anotherLogMessage.DateTime);
        }
    }
}
