﻿using System;
using Log.NLog;
using OctoCat.Test.Gui.ViewModels;
using OctoCat.Test.Log;

namespace OctoCat.Test.Gui.Design.ViewModels
{
    public class DesignMainWindowViewModel : MainWindowViewModel
    {
        public DesignMainWindowViewModel() : base(null, null, null, null, null, null)
        {
            Status = "Design status";
            Sum = 123567890;
            SkippedLinesCount = 95;
            SkippedFilesCount = 3;
            EmptyLinesCount = 50;
            CurrentProgressAmount = 7500;
            TotalProgressAmount = 10000;
            TimeElapsed = TimeSpan.FromMilliseconds(15585);
            Logs.Add(new NLogMessage(LogLevel.Debug, null, "Design debug message"));
            Logs.Add(new NLogMessage(LogLevel.Info, null, "Design debug message"));
            Logs.Add(new NLogMessage(LogLevel.Success, null, "Design debug message"));
            Logs.Add(new NLogMessage(LogLevel.Warn, null, "Design debug message"));
            Logs.Add(new NLogMessage(LogLevel.Error, null, "Design error message"));
        }
    }
}
