﻿using System;
using System.Windows;
using Catel.ExceptionHandling;
using Catel.IoC;
using Catel.Services;
using Log.NLog;
using OctoCat.Test.Gui.Settings;
using OctoCat.Test.Log;
using OctoCat.Test.NumericFiles;
using OctoCat.Test.Settings;

namespace OctoCat.Test.Gui
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            //Preloading assemblies
            //AppDomain.CurrentDomain.PreloadAssemblies();

            //Improving performance
            // For more information, see https://catelproject.atlassian.net/wiki/display/CTL/Performance+considerations
            Catel.Data.ModelBase.DefaultSuspendValidationValue = true;
            Catel.Windows.Controls.UserControl.DefaultCreateWarningAndErrorValidatorForViewModelValue = false;
            Catel.Windows.Controls.UserControl.DefaultSkipSearchingForInfoBarMessageControlValue = true;

            var serviceLocator = ServiceLocator.Default;
            serviceLocator.RegisterType<ISettingsService, SettingsService>();
            serviceLocator.RegisterType<INumericFileService, FileService>();
            serviceLocator.RegisterType<INumericFileFactory, FileFactory>();
            serviceLocator.RegisterType<ILogService, NLogService>();

            var dependencyResolver = this.GetDependencyResolver();
            var messageService = dependencyResolver.Resolve<IMessageService>();
            var logService = dependencyResolver.Resolve<ILogService>();
            var exceptionService = dependencyResolver.Resolve<IExceptionService>();
            exceptionService.Register<Exception>(exception =>
            {
                logService.Error(exception);
                messageService.ShowErrorAsync(exception.Message);
            }, null);

            base.OnStartup(e);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            // Get advisory report in console
            //ApiCopManager.AddListener(new ConsoleApiCopListener());
            //ApiCopManager.WriteResults();

            base.OnExit(e);
        }
    }
}