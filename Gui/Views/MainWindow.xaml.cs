﻿namespace OctoCat.Test.Gui.Views
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            LogsListBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("", System.ComponentModel.ListSortDirection.Descending));
        }
    }
}
