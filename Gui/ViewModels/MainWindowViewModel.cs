﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catel.Data;
using Catel.ExceptionHandling;
using Catel.MVVM;
using Catel.Services;
using OctoCat.Test.Log;
using OctoCat.Test.NumericFiles;

namespace OctoCat.Test.Gui.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        public override string Title => "OctoCat TEST";

        public TaskCommand GenerateCommand { get; }
        public TaskCommand CalculateCommand { get; }
        public Command CancelOperationCommand { get; }
        public Command SelectDirCommand { get; }

        private readonly IMessageService _messageService;
        private readonly ISelectDirectoryService _selectDirectoryService;
        private readonly IExceptionService _exceptionService;
        private readonly IDispatcherService _dispatcherService;
        private readonly ILogService _logService;
        private readonly INumericFileService _numericFileService;


        /// <summary>Строка состояния</summary>
        public string Status
        {
            get { return GetValue<string>(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }
        public static readonly PropertyData StatusProperty = RegisterProperty(nameof(Status), typeof(string));

        /// <summary>Выбранная директория для чтения или генерации</summary>
        public string SelectedDir
        {
            get { return GetValue<string>(SelectedDirProperty); }
            set { SetValue(SelectedDirProperty, value); }
        }
        public static readonly PropertyData SelectedDirProperty = RegisterProperty(nameof(SelectedDir), typeof(string));

        /// <summary>Сумма значений строк всех файлов</summary>
        public long Sum
        {
            get { return GetValue<long>(SumProperty); }
            set { SetValue(SumProperty, value); }
        }
        public static readonly PropertyData SumProperty = RegisterProperty(nameof(Sum), typeof(long));

        /// <summary>Количество пропущенных строк (строки, которые не удалось распарсить)</summary>
        public int SkippedLinesCount
        {
            get { return GetValue<int>(SkippedLinesCountProperty); }
            set { SetValue(SkippedLinesCountProperty, value); }
        }
        public static readonly PropertyData SkippedLinesCountProperty = RegisterProperty(nameof(SkippedLinesCount), typeof(int));

        /// <summary>Количество пустых строк</summary>
        public int EmptyLinesCount
        {
            get { return GetValue<int>(EmptyLinesCountProperty); }
            set { SetValue(EmptyLinesCountProperty, value); }
        }
        public static readonly PropertyData EmptyLinesCountProperty = RegisterProperty(nameof(EmptyLinesCount), typeof(int));

        /// <summary>Количество пропущенных файлов (файлы, которые не удалось распарсить)</summary>
        public int SkippedFilesCount
        {
            get { return GetValue<int>(SkippedFilesCountProperty); }
            set { SetValue(SkippedFilesCountProperty, value); }
        }
        public static readonly PropertyData SkippedFilesCountProperty = RegisterProperty(nameof(SkippedFilesCount), typeof(int));

        /// <summary>Обработанных файлов (в т.ч. пропущенных)</summary>
        public int CurrentProgressAmount
        {
            get { return GetValue<int>(CurrentProgressAmountProperty); }
            set { SetValue(CurrentProgressAmountProperty, value); }
        }
        public static readonly PropertyData CurrentProgressAmountProperty = RegisterProperty(nameof(CurrentProgressAmount), typeof(int));

        /// <summary>Всего файлов (обработанных и необработанных)</summary>
        public int TotalProgressAmount
        {
            get { return GetValue<int>(TotalProgressAmountProperty); }
            set { SetValue(TotalProgressAmountProperty, value); }
        }
        public static readonly PropertyData TotalProgressAmountProperty = RegisterProperty(nameof(TotalProgressAmount), typeof(int));

        /// <summary>Количество времени с начала операции</summary>
        public TimeSpan TimeElapsed
        {
            get { return GetValue<TimeSpan>(TimeElapsedProperty); }
            set { SetValue(TimeElapsedProperty, value); }
        }
        public static readonly PropertyData TimeElapsedProperty = RegisterProperty(nameof(TimeElapsed), typeof(TimeSpan));

        public ObservableCollection<ILogMessage> Logs { get; }
        private const int MaxLogMessages = 50;

        public MainWindowViewModel(
            IMessageService messageService,
            ISelectDirectoryService selectDirectoryService,
            IExceptionService exceptionService,
            IDispatcherService dispatcherService,
            ILogService logService,
            INumericFileService numericFileService)
        {
            _messageService = messageService;
            _selectDirectoryService = selectDirectoryService;
            _exceptionService = exceptionService;
            _dispatcherService = dispatcherService;
            _logService = logService;
            _numericFileService = numericFileService;
            GenerateCommand = new TaskCommand(Generate, CanGenerate);
            CalculateCommand = new TaskCommand(Calculate, CanCalculate);
            CancelOperationCommand = new Command(CancelOperation, CanCancelOperation);
            SelectDirCommand = new Command(OpenSelectDirDialog);

            Logs = new ObservableCollection<ILogMessage>();
        }

        #region Generation

        private bool CanGenerate()
        {
            return _numericFileService != null && !_numericFileService.IsBusy && !string.IsNullOrWhiteSpace(SelectedDir);
        }

        private async Task Generate(CancellationToken cancellationToken)
        {
            try
            {
                Status = "Preparing to generate";
                ResetValues();
                if (Directory.Exists(SelectedDir) && Directory.EnumerateFileSystemEntries(SelectedDir).Any())
                {
                    if (await _messageService.ShowAsync($"Directory \"{SelectedDir}\" is not empty. Clean directory and delete all files and folders?",
                            "Clean directory?", MessageButton.YesNo) == MessageResult.Yes)
                    {
                        Status = $"Cleaning directory \"{SelectedDir}\"...";
                        await _numericFileService.CleanDirAsync(SelectedDir);
                    }
                    else
                        return;
                }

                try
                {
                    await _numericFileService.GenerateAsync(SelectedDir, new Progress<GenerationProgressReport>(OnGenerationProgress), cancellationToken);
                }
                catch (OperationCanceledException)
                {
                    Status = "Generation was cancelled";
                }
            }
            catch (Exception exc)
            {
                _exceptionService.HandleException(exc);
            }
        }

        private void OnGenerationProgress(GenerationProgressReport progress)
        {
            _dispatcherService.BeginInvoke(() =>
            {
                Status = progress.CurrentProgressMessage;
                TotalProgressAmount = progress.TotalProgressAmount;
                CurrentProgressAmount = progress.CurrentProgressAmount;
                TimeElapsed = progress.TimeElapsed;
            });
        }

        #endregion

        #region Calculation

        private bool CanCalculate()
        {
            return _numericFileService != null && !_numericFileService.IsBusy && !string.IsNullOrWhiteSpace(SelectedDir);
        }

        private async Task Calculate(CancellationToken cancellationToken)
        {
            try
            {
                Status = "Preparing to calculate...";

                ResetValues();

                if (!Directory.Exists(SelectedDir))
                {
                    await _messageService.ShowWarningAsync($"Directory \"{SelectedDir}\" does not exist");
                    return;
                }
                if (!Directory.EnumerateFileSystemEntries(SelectedDir).Any())
                {
                    await _messageService.ShowWarningAsync($"Directory \"{SelectedDir}\" is empty");
                    return;
                }

                try
                {
                    await _numericFileService.CalculateAsync(SelectedDir, new Progress<CalculationProgressReport>(OnCalculationProgress), cancellationToken);
                }
                catch (OperationCanceledException)
                {
                    Status = "Calculation was cancelled";
                }
            }
            catch (Exception exc)
            {
                _exceptionService.HandleException(exc);
            }
        }

        private void OnCalculationProgress(CalculationProgressReport progress)
        {
            _dispatcherService.BeginInvoke(() =>
            {
                Status = progress.CurrentProgressMessage;
                CurrentProgressAmount = progress.CurrentProgressAmount;
                TotalProgressAmount = progress.TotalProgressAmount;
                Sum = progress.CurrentSum;
                EmptyLinesCount = progress.EmptyLinesCount;
                SkippedLinesCount = progress.SkippedLinesCount;
                SkippedFilesCount = progress.SkippedFilesCount;
                TimeElapsed = progress.TimeElapsed;
            });
        }

        #endregion

        private bool CanCancelOperation()
        {
            return _numericFileService != null && _numericFileService.IsBusy; ;
        }

        private void CancelOperation()
        {
            Status = "Cancelling...";
            if (CalculateCommand.IsExecuting)
                CalculateCommand.Cancel();
            if (GenerateCommand.IsExecuting)
                GenerateCommand.Cancel();
        }

        private void OpenSelectDirDialog()
        {
            if (!string.IsNullOrWhiteSpace(SelectedDir))
                _selectDirectoryService.InitialDirectory = SelectedDir;
            if (_selectDirectoryService.DetermineDirectory())
                SelectedDir = _selectDirectoryService.DirectoryName;
        }

        private void ResetValues()
        {
            Status = "";
            TimeElapsed = TimeSpan.Zero;
            CurrentProgressAmount = 0;
            EmptyLinesCount = 0;
            SkippedFilesCount = 0;
            SkippedLinesCount = 0;
            Sum = 0;
            TotalProgressAmount = 0;
        }

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            _logService.OnLogWritten += OnLogWritten;
        }

        private void OnLogWritten(object sender, LogEventArgs logEventArgs)
        {
            _dispatcherService.BeginInvoke(() =>
            {
                Logs.Add(logEventArgs.LogMessage);
                lock (Logs)
                {
                    if (Logs.Count > MaxLogMessages)
                    {
                        var earlestTime = Logs.Min(message => message.DateTime);
                        var earlestMessage = Logs.FirstOrDefault(message => message.DateTime == earlestTime);
                        Logs.Remove(earlestMessage);
                    }
                }
            });
        }

        protected override async Task CloseAsync()
        {
            // unsubscribe from events here
            await base.CloseAsync();
        }
    }
}
