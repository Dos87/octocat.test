﻿using Nerdle.AutoConfig;
using OctoCat.Test.Settings;

namespace OctoCat.Test.Gui.Settings
{
    public class SettingsService : ISettingsService
    {
        public IGeneralSettings GeneralSettings { get; }
        public IGeneratorSettings GeneratorSettings { get; }
        public ICalculatorSettings CalculatorSettings { get; }

        public SettingsService()
        {
            GeneralSettings = AutoConfig.Map<IGeneralSettings>();
            GeneratorSettings = AutoConfig.Map<IGeneratorSettings>();
            CalculatorSettings = AutoConfig.Map<ICalculatorSettings>();
        }
    }
}
